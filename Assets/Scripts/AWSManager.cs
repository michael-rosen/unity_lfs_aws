using UnityEngine;
using System.Linq;
using System.Collections;
using UnityEngine.UI;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.Runtime;
using System.IO;
using System;
using Amazon.S3.Util;
using System.Collections.Generic;
using Amazon.CognitoIdentity;
using Amazon;
using UnityEngine.Networking;

public class AWSManager : MonoBehaviour
{
    public string S3Region = RegionEndpoint.USEast2.SystemName;
    public string SampleName = "workshop";
    public string S3BucketName = "unity-s3-test-rosen";

    private AWSCredentials _credentials;
    private IAmazonS3 _s3Client;
    
    private RegionEndpoint _S3Region
    {
        get { return RegionEndpoint.GetBySystemName(S3Region); }
    }

    private AWSCredentials Credentials
    {
        get
        {
            if (_credentials == null)
                _credentials = new CognitoAWSCredentials(
                    "us-west-1:2819517d-105c-490e-bf73-83f25f174fa6", RegionEndpoint.USWest1
               );
            return _credentials;
        }
    }

    private IAmazonS3 Client
    {
        get
        {
            if (_s3Client == null)
            {
                _s3Client = new AmazonS3Client(Credentials, _S3Region);
            }
            //test comment
            return _s3Client;
        }
    }

    void Awake()
    {
        UnityInitializer.AttachToGameObject(this.gameObject);
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Debug.Log("clicked");
            //GetBucketList();
            GetObjects();
        }
    }
    private void GetBucketList()
    {
        Debug.Log("running bucket list");
        Client.ListBucketsAsync(new ListBucketsRequest(), (responseObject) =>
        {
            if (responseObject.Exception == null)
            {
                responseObject.Response.Buckets.ForEach((s3b) =>
                {
                    Debug.Log(string.Format("bucket = {0}, created date = {1} \n", s3b.BucketName, s3b.CreationDate));
                });
            }
            else
            {
                Debug.Log(responseObject.Exception);
            }
        });
    }


    public void GetObjects()
    {

        var request = new ListObjectsRequest()
        {
            BucketName = S3BucketName
        };

        Client.ListObjectsAsync(request, (responseObject) =>
        {
            if (responseObject.Exception == null)
            {
                bool assetFound = responseObject.Response.S3Objects.Any(obj => obj.Key == SampleName);
                Debug.Log("Asset found:" + assetFound);
                if (assetFound == true)
                {
                    StartCoroutine(DownloadBundleRoutine());
                }
            }
            else
            {
                Debug.Log(responseObject.Exception);
            }
        });
    }

    IEnumerator DownloadBundleRoutine()
    {
        Debug.Log("starting coroutine");
        string uri = "https://unity-s3-test-rosen.s3.us-east-2.amazonaws.com/workshop";
        using (UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(uri))
        {
            yield return request.SendWebRequest();
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
            String [] names = bundle.GetAllAssetNames();
            foreach (String str in names)
            {
                Debug.Log("Names:" + str);
            }
            GameObject workshop = bundle.LoadAsset<GameObject>("workshop set");
            Debug.Log(workshop);
            Instantiate(workshop, new Vector3(2.0f, 0, 0), Quaternion.identity);
        }
    }
}
